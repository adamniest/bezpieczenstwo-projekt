# BezpieczenstwoProjekt

Szyfrowanie obrazów z użyciem DES i SDES

W pliku `src/app/encrypt/services/encrypt-des.service.ts` znajduje się kod od algorytmu DES

W pliku `src/app/encrypt/services/encrypt-sdes.service.ts` znajduje się kod od algorytmu SDES

W pliku `src/app/encrypt/services/encryption.service.ts` znajduje się logika odpowiedzialna za dzielenie pliku na odpowiednie fragmenty.

Aby uruchomić aplikację należy zainstalować pakiety `npm install` oraz włączyć server `ng serve`

Ponadto aplikacja jest dostępna pod urlem: `https://bsi-images.web.app/`

